# GP Onion skin

Blender add-on, Enhanced Grease Pencil Onion Skin tab, providing world relative, transformable onion skin instances .

# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp-onion-skin/-/archive/master/gp-onion-skin-master.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.

# User guide

Open the Onion skin addon tab, in the N-tools of 3D viewport :
Not activated by default, it simply gather regular onion skin settings.

![guide](docs/gponion_01.png "guide")

- Object display mode shows like the native onion skin, relative to the object origin.

![guide](docs/gponion_02.png "guide")

- World display mode takes object animation into account, and shows instances at their world position.
- Camera display mode is useful while working with an animated camera, showing instances at their screen position.

![guide](docs/gponion_03.png "guide")

Note that the addon will see a keyframe even if only object animation is keyed (frame 20)

- By clicking on the transform hand modal operator, you can now transform the onion skin instance with usual G, R, S shortcuts.

![guide](docs/gponion_04.png "guide")

- Enabling the collection target mode will generate onion skin for every object in the active collection, allowing you to work with mutli-objects cut-out rigs.

![guide](docs/gponion_05.png "guide")



# Warning

This tool is in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)
