# ROADMAP
 TEST1: 2frame before to frame after one object

TODO NEXT:
   Improve strokeconfo. can't think of a way to solve this for the moemnt. time seems to be  linked to GP performances. hundreds of strokes to conform at each call and strokes conformations take around 0.003s per stroke

    MODAL_OS_MOVE
        - make child Scale   relative to parent pivot
        - make Child rotate  relative to parent pivot
        - make reset icon appear only when layer was moved    

    KEYFRAME MODE:
        - Rewrite get_before_frames() and get_after_frame() to make it more maintainable 
        - Make keyframe filter efficient     
    
    MAINTAINABILITY & STABILLITY     
        - TO FINISH Manage onion skin properties globally
        - make clear stable (probably linked to previous point)

    BUGS:
        -flip propertie sometimes create issues (seen once)

Patch 01: fix issue on 'OFF' mode

v0.1.4
    MODAL_OS_MOVE
        DONE allow transform on multiple layers
        DONE when TRAGET mode to CHILD, move apply to children layers too 
        DONE make grab relative to parent pivot


    USER XP:
        DONE add control over onion skin position> ghost_in_front checkbox        
        DONE add option to refresh on object selection change
        DONE VERY UNSTABLE add clear button to remove ghost objects and collection. seems crashy

    BUGS:
        SOLVED on first launch create ghost on empty frames
        REMOVED old op_generate() called on addon activation creating issues 
        REMOVED still searching if native_os is on while it doesnt make sense anymore
        SOLVED pin not working on ik rig after v80
        SOLVED if crash during semaphore HOLD, plug-in isn't responsive anymore. Added a semaphore reset on 'Disable' Mode.
        SOLVED Avoid Z fight  and allow to put Ghost and/or OnionSkined object in front

    

v0.1.3
    OS GHOST GENERATION
        DONE Improve set_coordinates (from 6s to less than 0.3s for 6 ghost fullik rig) .Around 70% of processing time. ~~May be enough to  make 'CHILD' option usable~~ Nope...
            DONE simplified and merge the 3 set_ccordinate functions into one
            DONE limit calculation to visible frames only
            DONE get rid of this pattern : set frame cible> get matrix_world > set frame back...it is super slow (or at least only once per frame)
        DONE store opacity in properties, not in a library global variable
        KEYFRAME MODE:
            DONE Make 'keyframe' mode available       
        
    OS_PANEL
        DONE Add a 'PIN' option to keep onion skin on choosen layers when they are no longer selected
        DONE use OS_DATA to store displayed frames instead of recalculating this in draw()
    
    MODAL_OS_MOVE
        DONE based on choosen layer, not context object       

v0.1.2:
    CLEANUP & PERFORMANCES
        -DONE Reorganise to fit AAT standart and clean up
        -DONE replace what can be by Andarta's common def
        -DONE stop using native OS -Too rigid and slow- and use a custom material instead
        -DONE get rid of the OS_OT_GENERATE operator doing the same thing with just a change of parameter.  shoud be a class  function call on settings update
        -DONE get rid of OS_OT_USE_OS that shoud be a class function call on settings update
        -DONE get read of OS_OT_CLEAR operator that is only triggered by mode change.It should be a class function triggered by   settings update
        -WIP create a hash_utils.py library to store gp objects hash a be able to tell if somethingwas altered or not.
        -DONE  remove bpy.ops.gpencil.layer_merge() from create_layer_per_frame
        -DONE  get rid of  both complete_with_frames_before_after and create_layer_per_frame. This create a copy, then merge the layer of the copy then create another GP with renamed layers. way too expensive in cpu time mean execute time 0.3s TEST1 protocol
        -DONE Do not create from scratch ghosts alreay existing
        -DONE Avoid dopesheet_refresh method . Very slow: mean execute time 0.3s on TEST1 protocol (and probably useless with new copy methods)
        -DONE Remove functions: add_frames_to_ghost ,update_ghost_before_range,update_ghost_after_range. It's is very complicated, while it should just be a call of update function on settings change        
        -WIP Manage GP_OS propertie globally, not with a property collection per GP. Intricated with tranform method, process with care!
        -DONE put selected object in front. Remove and eventually make it optionnal alter
        -DONE Add a string PATTERN check to avoid layer that aren't stroke or line. Avoid shadow and mask
        WIP Improve set_coordinates . Around 90% of processing time. May be enough to  make 'CHILD' available:
            - DONE this is very slow, need to find better solution, or at lest not call it for each ghost
            while frame_handler in bpy.app.handlers.frame_change_post :
            bpy.app.handlers.frame_change_post.remove(frame_handler)    (replaced by semaphore system)

        

    FEATURE
        DONE Sort layers between LINE and FILL based on string pattern and apply a different material so that fill and stroke addon option only applies on appropriate strokes
        DONE add CHILD target mode. Display Onio skin for selected object + there children. PARTIALLY SOLVED Issue: will be slow as long as there isn't a proper method to avoid unnecessary opertation. Still slow mainly because of coordinate calculation
        DONE change object target to SELECTED. calculate for every selected object. SOLVED Issue: somthing in the old code seems to be overwritting selected object. Need to fin where
    


    

   

    BUGS:
        SOLVED BUG Duplicates rigs element with there constraints and modifiers, leading to absurd transform
        SOLVED in_front cancellation is sometimes off
        SOLVED BUG first layer opacity  is 1.0 on refresh...why? 
        SOLVED BUG messes with z src gpencil position    
        SOLVED BUG conflict with AAT/draw 'ReferenceError: StructRNA of type Object has been removed' making it impossible to change object selection once triggered. Maybe solved with a more recent AAT
        

    TO IMPROVE:        
         WIP to many redefinition of attributes between source, UI and ghosts. Will lead to instability,slowness and difficulties to maintain the addon. ghost attribute should be linked once and for all to theyr source data. Source should be the only element modified by UI.
         NOT SOLVED but avoided fith layer filter BUG masking not applied

    USERXP:
        IMPROVED TO 0.02sec: huge performance issues making it impossible to use. 0.6 sec per refresh with a single object     
        SOLVED Need to select objet/click on X to remove unwanted obj OS> painfull
        SOLVED OnionSkin not updating on object change and not removing old ghost seams unnatural. 


# Use case:
du côté 'Onion skin en refresh manuel' mon petit sondage indique que ce serait pas franchement l'idéal, les usecase les plus classiques sont devellopper une marche faite sur place, recaler un élément en contranimant, vérifier le respect des courbes de trajectoires, dans ces 3 cas là on active l'onion skin pour travailler sur un ensemble de frames les unes par rapport aux autres, plutot que sur une seule pendant longtemps

# Known bugs and limitations
## Bugs
- An undo or redo user action (Ctrl Z or Shift Ctrl Z) that causes the current step in undo history to become a frame change event can create problems. It can lead to a wrong display of ghost frames. In this case, in order to restore the correct display and data, either change frame again or disable and enable again the onion skin.

- cliquer sur la croix pour désactiver l'onion skin, ne desactive pas le mode (world/object/camera)

-l'operateur pour supprimer les transformations d'une frame souscrit à REGISTER et ne devrait pas

- en mode collection les sliders d'opacité et les before and after,  n'affectent que l'objet actif (ils devraient agir sur tous les objets de la collection active)


## Limitations
- This onion skin doesn't take masks into account. Even if the source object has layers with masks, the corresponding ghost object will appear as if no mask existed.


## To do

- il manque un bouton pour reset tous les transforms (à placer à la place de l'icone play sur la ligne de la frame active)

- le bouton pour activer/desactiver l'onion skin devrait être déplacé au bout de la ligne de la frame active (icone oeil)

- le premier slider d'opacité générale peut être enlevé car déjà à la ligne de la frame active




import bpy
import time


need_to_update = False
last_length = 0
handlers = bpy.app.handlers.scene_update_post


def select_assembly(scene):
	global last_length, need_to_update


	if last_length != len(bpy.context.selected_objects):
		need_to_update = True
		last_length = len(bpy.context.selected_objects)


	if need_to_update == True:
		print("Updated:", time.time())
		need_to_update = False


def add_handler(handlers, func):
	c = 0
	r = False
	for i in handlers:
		if i.__name__ == func.__name__:
			handlers.remove(handlers[c])
			r = True
		c += 1
	handlers.append(func)
	print(("Added" if r == False else "Replaced")+" Handler:", i.__name__)
add_handler(handlers, select_assembly)