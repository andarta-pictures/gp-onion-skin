import time

from .common.general_utils import get_objects_childs_dic
from .common.gp_utils import get_gplyr_from_name, get_material_by_name
from .common.utils import register_classes, unregister_classes
import bpy
from .core import get_ghost, get_kf_index_from_nb, get_surroundings_ghost_layers, ghost_opacity_gradient, set_parent_name

############ UI LAYOUT ############ 

class OS_PT_PANEL(bpy.types.Panel):
   
    bl_label = "Onion Skin addon"
    bl_idname = "OS_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Onion Skin"

   

    def draw(self, context):
        verbose = False
        if verbose:
            print("Start drawing OS UI")
            start_time = time.time()
        # src_ob = bpy.context.object
        os_data = bpy.context.scene.osdata
        # if src_ob is not None : 
        #     if src_ob.type == 'GPENCIL' :
                


        #         src_obj_data = src_ob.data
        
        layout = self.layout
        scene = context.scene
        row = layout.row()
        row.prop(scene.osdata, 'refresh_on_select',text='WIP! Refresh on selection')
        row = layout.row()

        row.label(text = 'OnionSkin mode:')
        row.prop(scene.osdata, 'mode', expand=True)
        row = layout.row()
        if scene.osdata.mode == 'DIS':
            row.operator('os.clear', icon = 'X', text = 'Clear onion skin collection')
            return
        row = layout.row()
        #layout for target mode (OS for object or for collection)
        row.label(text = 'Target mode:')
        row.prop(scene.osdata, 'target', expand=True)
                        

        
        
        col = layout.column()
        col.label(text = 'Frame mode:')
        col.prop(os_data, "onion_mode", expand=True)
        if os_data.onion_mode == 'Keyframes':
            col.prop(os_data, "filter_kf_type")
        col.prop(os_data, "cur_onion_factor", text="Opacity", slider=True)
        row = layout.row()
        row.prop(os_data, "force_in_front", text="Obj in front")
        row.prop(os_data, "ghost_in_front", text="Ghost in front")
        row = layout.row()
        row.prop(os_data, "ghost_before_range", text="Before")
        row.prop(os_data, "ghost_after_range", text="After")

        before_material = os_data.before_material
        after_material = os_data.after_material
        before_fill_material = os_data.before_fill_material
        after_fill_material = os_data.after_fill_material

                        
        split = layout.split() 
        if before_material is not None and after_material is not None:
            col = split.column()   
            #get stroke colors from the material
            # col.label(text="Before")
            col.prop(before_material.grease_pencil, "show_stroke", text="stroke")
            if before_material.grease_pencil.show_stroke:
                col.prop(before_material.grease_pencil, "color", text="")
            col.prop(before_fill_material.grease_pencil, "show_fill", text="fill")
            if before_fill_material.grease_pencil.show_fill:
                col.prop(before_fill_material.grease_pencil, "fill_color", text="")
        

            # col.prop(before_material, "before_color", text="")
            col = split.column()
            # col.label(text="After")
            col.prop(after_material.grease_pencil, "show_stroke", text="stroke")
            if after_material.grease_pencil.show_stroke:
                col.prop(after_material.grease_pencil, "color", text="")
            col.prop(after_fill_material.grease_pencil, "show_fill", text="fill")
            if after_fill_material.grease_pencil.show_fill:
                col.prop(after_fill_material.grease_pencil, "fill_color", text="")

        # col.prop(gpd, "after_color", text="")
        layout = self.layout

        #layout of the 'move onion' part
        opacity_list_center_index = len(ghost_opacity_gradient) / 2
        box = layout.box()
        # if ghost_obj is not None and len(ghost_obj_data.layers) > 0:
        src_objs = []
        src_objs = bpy.context.selected_objects

        for pinned_obj_key in os_data.get_pinned_objects_keys():
            #get the pinned object
            pinned_obj = bpy.data.objects.get(pinned_obj_key)
            if pinned_obj is not None:
                #if not selected, add it to the list of selected objects
                if pinned_obj not in src_objs:
                    src_objs.append(pinned_obj)


        #for every selected object
        for src_obj in src_objs :
            ghost_obj = get_ghost(src_obj)
            #layout of the onion skin settings
            if ghost_obj is not None :
                ghost_obj_data = ghost_obj.data
                # gpd = ghost_obj_data
            else :
                continue
                pass
                # gpd = src_obj_data
            row = box.row()

            row.prop( ghost_obj_data.os_src_ob_data, "expanded",
                        icon="TRIA_DOWN" if ghost_obj_data.os_src_ob_data.expanded else "TRIA_RIGHT",
                        icon_only=True, emboss=False
                    )
            
            row.label(text="" + ghost_obj.name)
            
            row.prop(src_obj.data.os_src_ob_data, "pinned",
                        icon="PINNED" if src_obj.data.os_src_ob_data.pinned else "UNPINNED",
                        icon_only=True, emboss=False)
            if ghost_obj_data.os_src_ob_data.expanded:
                #operator to reset opacities
                # row = box.row()
                # row.operator('os.reset_opacities', icon = 'MOD_OPACITY', emboss = True)
                
                #NEW
                # ghost_layers_names_before,ghost_layers_names_after = get_surroundings_ghost_layers(ghost_obj, 
                #                                                 context.scene.frame_current, 
                #                                                 os_data.ghost_before_range, 
                #                                                 os_data.ghost_after_range,
                #                                                 #    mode=ghost_obj_data.onion_mode
                #                                                 )
                ghost_layers_names_before= list(os_data.displayed_frames_before[0:os_data.ghost_before_range])
                                        
                ghost_layers_names_after= list(os_data.displayed_frames_after[0:os_data.ghost_after_range])
                ghost_layers_names_before.reverse()
                frames_list = ghost_layers_names_before+ [context.scene.frame_current]+ ghost_layers_names_after
                # frames_list.sort()
                for frame_index in frames_list:

                    if frame_index == context.scene.frame_current:
                        row = box.row()
                        row.label(text = "", icon = 'DOT')
                        delete = row.operator('os.delete_transforms', icon = "X", text = '', emboss = False)
                        row.prop(ghost_obj_data, "onion_factor", text = "Opacity", slider = True)
                    elif frame_index!=None:
                        ghost_layer = get_gplyr_from_name(ghost_obj, str(frame_index))
                        if ghost_layer is not None:
                            frame_nb = frame_index
                            row = box.row()
                            move = row.operator('os.move', icon = 'VIEW_PAN', text = "", emboss = False)
                            move.fr_num = frame_nb
                            ghost_names =  ghost_obj.name+','
                            # if os_data.target == 'SELECTED':
                            #     move.current_ghost_name = ghost_obj.name
                            if os_data.target == 'CHILD':
                                # ghost_names = ''
                                for child in get_objects_childs_dic([src_obj],bpy.data.objects,add_self=False,recursive_dic={}).values():
                                    child_ghost = get_ghost(child)
                                    ghost_names += child_ghost.name+','
                            move.current_ghost_name = ghost_names
                            delete = row.operator('os.delete_frame_transform', icon = "X", text = '', emboss = False)
                            delete.fr_num = frame_nb

                            row.prop(ghost_layer,"opacity", expand = True, slider = True)
                            row.prop(ghost_layer,"hide", icon_only = True, emboss = False)
                        else:
                            row = box.row()
                            row.label(text = "No keyframe at %s on this element"%frame_index, icon = 'X')

        if verbose:
            print("End drawing OS UI")
            print("Time elapsed: ", time.time() - start_time)
    


classes = [
    OS_PT_PANEL
    
]

def register() :
    register_classes(classes)

def unregister() :
    unregister_classes(classes)